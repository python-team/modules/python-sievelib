python-sievelib (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2
  * Remove dependency on python3-pkg-resources (Closes: #1083707)
  * Fix SyntaxWarning (Closes: #1086961)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 31 Jan 2025 18:19:40 +0100

python-sievelib (1.4.1-1) unstable; urgency=low

  [ Alexandre Detiste ]
  * remove extraneous dependency on python3-six

  [ Michael Fladischer ]
  * New upstream version 1.4.1
  * Refresh patches.
  * Build using pybuild-plugin-pyproject and dh-sequence-python3.
  * Bump Standards-Version to 4.7.0.
  * Update year in d/copyright.
  * Add python3-typing-extensions to Build-Depends, required by tests.

 -- Michael Fladischer <fladi@debian.org>  Thu, 18 Jul 2024 12:53:01 +0000

python-sievelib (1.2.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.6.0.1.
  * Update year in d/copyright.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests and build.

 -- Michael Fladischer <fladi@debian.org>  Fri, 14 Jan 2022 08:36:57 +0000

python-sievelib (1.2.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.5.0.
  * Set Rules-Requires-Root: no.
  * Add d/upstream/metadata.

 -- Michael Fladischer <fladi@debian.org>  Sun, 25 Oct 2020 18:30:31 +0100

python-sievelib (1.1.1-3) unstable; urgency=low

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Michael Fladischer ]
  * Remove Python2 support.

 -- Michael Fladischer <fladi@debian.org>  Fri, 26 Jul 2019 10:46:26 +0200

python-sievelib (1.1.1-2) unstable; urgency=medium

  * Add python(3)-pkg-resources to Depends (Closes: #922343).

 -- Michael Fladischer <fladi@debian.org>  Tue, 30 Apr 2019 14:13:53 +0200

python-sievelib (1.1.1-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Michael Fladischer ]
  * New upstream release.
  * Bump Standards-Version to 4.3.0.
  * Bump debhelper compatibility and version to 12.
  * Add python(3)-pip and python(3)-setuptools-scm to Build-Depends as
    upstream now uses them in setup.py.
  * Mock version number for setuptools_scm using pkg-info.mk.
  * Add patch to fix import of req module from pip (>= 10).

 -- Michael Fladischer <fladi@debian.org>  Thu, 07 Feb 2019 11:38:46 +0100

python-sievelib (1.1.0-1) unstable; urgency=low

  * New upstream release.
  * Always use pristine-tar.
  * Bump debhelper compatibility and version to 11.
  * Bump Standards-Version to 4.1.3.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Tue, 09 Jan 2018 08:08:36 +0100

python-sievelib (1.0.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release.
  * Use https:// for copyright-format 1.0 URL.
  * Bump Standards-Version to 4.1.1.
  * Clean up modified files to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Wed, 01 Nov 2017 16:16:50 +0100

python-sievelib (0.9.2-1) unstable; urgency=low

  * New upstream release.
  * Initialize git-dpm config
  * Update package name in URLs for Vcs-* fields.
  * Disable tests as a file used by them is missing in the upstream tarball.
  * Clean files in sievelib.egg-info/.

 -- Michael Fladischer <fladi@debian.org>  Fri, 11 Dec 2015 12:58:38 +0100

python-sievelib (0.8+git20150929.71f2dee-1) unstable; urgency=low

  * Initial release (Closes: #800437).

 -- Michael Fladischer <fladi@debian.org>  Tue, 28 Apr 2015 17:35:01 +0200
